import sys
import calcoohija


class Calcplus(calcoohija.CalculadoraHija):
    pass


if __name__ == "__main__":
    fichero = open(sys.argv[1])
    elementos = fichero.readlines()
    for num in elementos:
        line = num.split(",")
        pos = 1
        resultado = 0
        objeto = Calcplus(int(line[pos]), int(line[pos + 1]))
        tamano = len(line) - 1

        while pos < tamano:
            if pos == 1:
                if line[0] == "suma":
                    resultado = objeto.plus(int(line[pos]), int(line[pos+1]))
                elif line[0] == "resta":
                    resultado = objeto.minus(int(line[pos]), int(line[pos+1]))
                elif line[0] == "multiplica":
                    resultado = objeto.mult(int(line[pos]), int(line[pos+1]))
                elif line[0] == "divide":
                    resultado = objeto.div(int(line[pos]), int(line[pos+1]))
                else:
                    sys.exit("Solo sumar, restar, multiplicar o dividir.")
                pos = pos + 1
            else:
                objeto.op1 = resultado
                objeto.op2 = line[pos + 1]
                if line[0] == "suma":
                    resultado = objeto.plus(int(objeto.op1), int(objeto.op2))
                elif line[0] == "resta":
                    resultado = objeto.minus(int(objeto.op1), int(objeto.op2))
                elif line[0] == "por":
                    resultado = objeto.mult(int(objeto.op1), int(objeto.op2))
                elif line[0] == "entre":
                    resultado = objeto.div(int(objeto.op1), int(objeto.op2))
                else:
                    sys.exit("Solo sumar, restar, multiplicar o dividir.")
                pos = pos + 1
        print(int(resultado))
