import sys


class Calculadora():
    def __init__(self, operando1, operando2):
        self.valor1 = operando1
        self.valor2 = operando2

    def plus(self):
        return self.valor1 + self.valor2


    def minus(self):
        return self.valor1 - self.valor2


class CalculadoraHija(Calculadora):
    def mult(self):
        return self.valor1 * self.valor2


    def div(self):
        try:
            return self.valor1 / self.valor2
        except ZeroDivisionError:
            sys.exit("No se puede dividir entre 0")



if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    objeto = CalculadoraHija(operando1, operando2)

    if sys.argv[2] == "suma":
        result = objeto.plus()
    elif sys.argv[2] == "resta":
        result = objeto.minus()
    elif sys.argv[2] == "por":
        result = objeto.mult()
    elif sys.argv[2] == "entre":
        if operando2 == "0":
            sys.exit('No se puede dividir entre 0')
        else:
            result = objeto.div()
    else:
        sys.exit('Operación sólo puede ser sumar, restar, multiplicar o dividir.')
    print(result)
