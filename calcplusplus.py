import sys
import calcoohija
import csv


class Calcplus(calcoohija.CalculadoraHija):
    pass


with open(sys.argv[1], newline="") as archivo:
    archivo = csv.reader(archivo)
    for num in archivo:

        pos = 1
        valor = 0
        objeto = Calcplus(int(num[lugar]), int(num[lugar + 1]))
        tamano = len(num) - 1

        while pos < tamano:
            if pos == 1:
                if num[0] == "suma":
                    valor = objeto.plus(int(num[pos]), int(num[pos+1]))
                elif num[0] == "resta":
                    valor = objeto.minus(int(num[pos]), int(num[pos+1]))
                elif num[0] == "por":
                    valor = objeto.mult(int(num[pos]), int(num[pos+1]))
                elif num[0] == "entre":
                    valor = objeto.div(float(num[pos]), float(num[pos+1]))
                else:
                    sys.exit("Solo sumar, restar, multiplicar o dividar.")
                lugar = lugar + 1
            else:
                objeto.op1 = valor
                objeto.op2 = num[pos + 1]
                if num[0] == "suma":
                    valor = objeto.plus(int(objeto.op1), int(objeto.op2))
                elif num[0] == "resta":
                    valor = objeto.minus(int(objeto.op1), int(objeto.op2))
                elif num[0] == "por":
                    valor = objeto.mult(int(objeto.op1), int(objeto.op2))
                elif num[0] == "entre":
                    valor = objeto.div(float(objeto.op1), float(objeto.op2))
                else:
                    sys.exit("Solo sumar, restar, multiplicar o dividar.")
                pos = pos + 1
        print(int(valor))
